#!/bin/sh
#
# Run all node-exporter-scripts.
#

scripts_dir="/etc/prometheus/node-exporter-scripts"
output_dir="/var/lib/prometheus/node-exporter"

# Be sure to drop the current temporary file (global variable) even if
# terminated while we're running scripts.
tmp_file=
trap "rm -f ${tmp_file} 2>/dev/null" EXIT INT TERM

# Exit with a non-zero status if any script errors out.
exit_status=0

# Run each node-exporter script, with a 120s timeout, in sequence.
for script in $(run-parts --regex='^[a-z0-9].*' --list "${scripts_dir}"); do
    script_basename=$(basename "${script}")
    script_name="${script_basename%.*}"
    output_file="${output_dir}/${script_name}.prom"
    tmp_file="${output_file}.tmp.$$"

    timeout 120 "${script}" > "${tmp_file}"
    rc=$?
    case $rc in
        0)
            mv -f "${tmp_file}" "${output_file}"
            echo "${script_basename}: saved to ${output_file}"
            ;;
        124)
            echo "error: ${script_basename}: timed out" >&2
            exit_status=$(( ${exit_status} + 1 ))
            ;;
        *)
            echo "error: ${script_basename}: failed (exit status ${rc})" >&2
            exit_status=$(( ${exit_status} + 1 ))
            ;;
    esac

    rm -f "${tmp_file}"
done

exit ${exit_status}
