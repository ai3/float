#!/bin/sh
test -x /usr/sbin/ipmi-sensors || exit 0
test -e /usr/lib/float/node-exporter-freeipmi.awk || exit 0
/usr/sbin/ipmi-sensors | awk -f /usr/lib/float/node-exporter-freeipmi.awk
